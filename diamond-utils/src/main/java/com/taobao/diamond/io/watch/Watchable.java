package com.taobao.diamond.io.watch;

public abstract interface Watchable
{
  public abstract WatchKey register(WatchService paramWatchService, WatchEvent.Kind<?>[] paramArrayOfKind);
}

