 package com.taobao.diamond.io.watch;

 public class WatchEvent<T>
 {
   private Kind<T> kind;
   private int count;
   private T context;

   public WatchEvent(Kind<T> kind, int count, T context)
   {
     this.kind = kind;
     this.count = count;
     this.context = context;
   }

   public Kind<T> kind()
   {
     return this.kind;
   }

   public int count()
   {
     return this.count;
   }

   public T context()
   {
     return this.context;
   }

   public static abstract interface Kind<T>
   {
     public abstract String name();

     public abstract Class<T> type();
   }
 }
