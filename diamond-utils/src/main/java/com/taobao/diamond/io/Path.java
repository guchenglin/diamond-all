 package com.taobao.diamond.io;

 import com.taobao.diamond.io.watch.WatchEvent;
 import com.taobao.diamond.io.watch.WatchEvent.Kind;
 import com.taobao.diamond.io.watch.WatchKey;
 import com.taobao.diamond.io.watch.WatchService;
 import com.taobao.diamond.io.watch.Watchable;
 import java.io.File;
 import java.io.FileFilter;
 import java.io.FilenameFilter;
 import java.io.IOException;
 import java.net.URI;

 public class Path
   implements Watchable
 {
   private File file;

   public Path(File file)
   {
     this.file = file;
   }

   public File getFile()
   {
     return this.file;
   }

   public boolean canExecute()
   {
     return this.file.canExecute();
   }

   public boolean canRead()
   {
     return this.file.canRead();
   }

   public boolean canWrite()
   {
     return this.file.canWrite();
   }

   public int compareTo(File pathname)
   {
     return this.file.compareTo(pathname);
   }

   public boolean createNewFile() throws IOException
   {
     return this.file.createNewFile();
   }

   public boolean delete()
   {
     return this.file.delete();
   }

   public void deleteOnExit()
   {
     this.file.deleteOnExit();
   }

   public boolean equals(Object obj)
   {
     return this.file.equals(obj);
   }

   public boolean exists()
   {
     return this.file.exists();
   }

   public File getAbsoluteFile()
   {
     return this.file.getAbsoluteFile();
   }

   public String getAbsolutePath()
   {
     return this.file.getAbsolutePath();
   }

   public File getCanonicalFile() throws IOException
   {
     return this.file.getCanonicalFile();
   }

   public String getCanonicalPath() throws IOException
   {
     return this.file.getCanonicalPath();
   }

   public long getFreeSpace()
   {
     return this.file.getFreeSpace();
   }

   public String getName()
   {
     return this.file.getName();
   }

   public String getParent()
   {
     return this.file.getParent();
   }

   public File getParentFile()
   {
     return this.file.getParentFile();
   }

   public String getPath()
   {
     return this.file.getPath();
   }

   public long getTotalSpace()
   {
     return this.file.getTotalSpace();
   }

   public long getUsableSpace()
   {
     return this.file.getUsableSpace();
   }

   public int hashCode()
   {
     return this.file.hashCode();
   }

   public boolean isAbsolute()
   {
     return this.file.isAbsolute();
   }

   public boolean isDirectory()
   {
     return this.file.isDirectory();
   }

   public boolean isFile()
   {
     return this.file.isFile();
   }

   public boolean isHidden()
   {
     return this.file.isHidden();
   }

   public long lastModified()
   {
     return this.file.lastModified();
   }

   public long length()
   {
     return this.file.length();
   }

   public String[] list()
   {
     return this.file.list();
   }

   public String[] list(FilenameFilter filter)
   {
     return this.file.list(filter);
   }

   public File[] listFiles()
   {
     return this.file.listFiles();
   }

   public File[] listFiles(FileFilter filter)
   {
     return this.file.listFiles(filter);
   }

   public File[] listFiles(FilenameFilter filter)
   {
     return this.file.listFiles(filter);
   }

   public boolean mkdir()
   {
     return this.file.mkdir();
   }

   public boolean mkdirs()
   {
     return this.file.mkdirs();
   }

   public boolean renameTo(File dest)
   {
     return this.file.renameTo(dest);
   }

   public boolean setExecutable(boolean executable, boolean ownerOnly)
   {
     return this.file.setExecutable(executable, ownerOnly);
   }

   public boolean setExecutable(boolean executable)
   {
     return this.file.setExecutable(executable);
   }

   public boolean setLastModified(long time)
   {
     return this.file.setLastModified(time);
   }

   public boolean setReadable(boolean readable, boolean ownerOnly)
   {
     return this.file.setReadable(readable, ownerOnly);
   }

   public boolean setReadable(boolean readable)
   {
     return this.file.setReadable(readable);
   }

   public boolean setReadOnly()
   {
     return this.file.setReadOnly();
   }

   public boolean setWritable(boolean writable, boolean ownerOnly)
   {
     return this.file.setWritable(writable, ownerOnly);
   }

   public boolean setWritable(boolean writable)
   {
     return this.file.setWritable(writable);
   }

   public String toString()
   {
     return this.file.toString();
   }

   public URI toURI()
   {
     return this.file.toURI();
   }

   public WatchKey register(WatchService watcher, WatchEvent.Kind<?>[] events)
   {
     return watcher.register(this, events);
   }
 }

