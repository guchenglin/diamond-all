 package com.taobao.diamond.io.watch;

 import com.taobao.diamond.io.Path;

 public class StandardWatchEventKind
 {
   public StandardWatchEventKind() {
   }

   public static final WatchEvent.Kind<Path> ENTRY_CREATE = new WatchEvent.Kind() {
     public String name() {
       return "ENTRY_CREATE";
     }

     public Class<Path> type()
     {
       return Path.class;
     }
   };

   public static final WatchEvent.Kind<Path> ENTRY_DELETE = new WatchEvent.Kind() {
     public String name() {
       return "ENTRY_DELETE";
     }

     public Class<Path> type()
     {
       return Path.class;
     }
   };

   public static final WatchEvent.Kind<Path> ENTRY_MODIFY = new WatchEvent.Kind() {
     public String name() {
       return "ENTRY_MODIFY";
     }

     public Class<Path> type()
     {
       return Path.class;
     }
   };

   public static final WatchEvent.Kind<Void> OVERFLOW = new WatchEvent.Kind() {
     public String name() {
       return "OVERFLOW";
     }

     public Class<Void> type()
     {
       return Void.class;
     }
   };
 }

