package com.taobao.diamond.io.watch.util;

import com.taobao.diamond.io.Path;
import java.util.LinkedList;
import java.util.List;

public class PathNode
{
  private List<PathNode> children;
  public Path path;
  private final boolean root;
  private long lastModified;

  public PathNode(Path path, boolean root)
  {
    this.children = new LinkedList();
    this.path = path;
    this.root = root;
    this.lastModified = path.lastModified();
  }

  public long lastModified()
  {
    return this.lastModified;
  }

  public boolean isRoot()
  {
    return this.root;
  }

  public List<PathNode> getChildren()
  {
    return this.children;
  }

  public void setPath(Path path)
  {
    this.path = path;
    this.lastModified = path.lastModified();
  }

  public Path getPath()
  {
    return this.path;
  }

  public void addChild(PathNode node)
  {
    this.children.add(node);
  }

  public void removeChild(PathNode node)
  {
    this.children.remove(node);
  }
}
