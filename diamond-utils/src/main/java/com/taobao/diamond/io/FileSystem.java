package com.taobao.diamond.io;

import com.taobao.diamond.io.watch.WatchService;

public class FileSystem
{
  private static final FileSystem instance = new FileSystem();

  private String interval = System.getProperty("diamon.watch.interval", "5000");

  public void setInterval(String interval)
  {
    this.interval = interval;
  }

  public static final FileSystem getDefault()
  {
    return instance;
  }

  public WatchService newWatchService()
  {
    return new WatchService(Long.valueOf(this.interval).longValue());
  }
}
