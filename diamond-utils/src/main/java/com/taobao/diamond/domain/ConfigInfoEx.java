 package com.taobao.diamond.domain;

 public class ConfigInfoEx extends ConfigInfo
 {
   private static final long serialVersionUID = -1L;
   private int status;
   private String message;

   public ConfigInfoEx()
   {
   }

   public ConfigInfoEx(String dataId, String group, String content)
   {
     super(dataId, group, content);
   }

   public int getStatus()
   {
     return this.status;
   }

   public void setStatus(int status)
   {
     this.status = status;
   }

   public String getMessage()
   {
     return this.message;
   }

   public void setMessage(String message)
   {
     this.message = message;
   }

   public String toString()
   {
     return "ConfigInfoEx [status=" + this.status + ", message=" + this.message + ", getDataId()=" + getDataId() + ", getGroup()=" + getGroup() + ", getContent()=" + getContent() + ", getMd5()=" + getMd5() + "]";
   }
 }

