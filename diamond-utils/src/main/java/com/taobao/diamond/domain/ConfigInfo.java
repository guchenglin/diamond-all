package com.taobao.diamond.domain;

import com.taobao.diamond.md5.MD5;
import java.io.PrintWriter;
import java.io.Serializable;

public class ConfigInfo
  implements Serializable, Comparable<ConfigInfo>
{
  static final long serialVersionUID = -1L;
  private String dataId;
  private String content;
  private String md5;
  private String group;
  private long id;

  public ConfigInfo()
  {
  }

  public ConfigInfo(String dataId, String group, String content)
  {
    this.dataId = dataId;
    this.content = content;
    this.group = group;
    if (this.content != null)
      this.md5 = MD5.getInstance().getMD5String(this.content);
  }

  public long getId()
  {
    return this.id;
  }

  public void setId(long id)
  {
    this.id = id;
  }

  public String getDataId()
  {
    return this.dataId;
  }

  public void setDataId(String dataId)
  {
    this.dataId = dataId;
  }

  public String getGroup()
  {
    return this.group;
  }

  public void setGroup(String group)
  {
    this.group = group;
  }

  public String getContent()
  {
    return this.content;
  }

  public void setContent(String content)
  {
    this.content = content;
  }

  public void dump(PrintWriter writer)
  {
    writer.write(this.content);
  }

  public String getMd5()
  {
    return this.md5;
  }

  public void setMd5(String md5)
  {
    this.md5 = md5;
  }

  public int compareTo(ConfigInfo o)
  {
    if (o == null) {
      return 1;
    }
    if ((this.dataId == null) && (o.getDataId() != null))
      return -1;
    int cmpDataId = this.dataId.compareTo(o.getDataId());
    if (cmpDataId != 0) {
      return cmpDataId;
    }
    if ((this.group == null) && (o.getGroup() != null))
      return -1;
    int cmpGroup = this.group.compareTo(o.getGroup());
    if (cmpGroup != 0) {
      return cmpGroup;
    }
    if ((this.content == null) && (o.getContent() != null))
      return -1;
    int cmpContent = this.content.compareTo(o.getContent());
    if (cmpContent != 0) {
      return cmpContent;
    }
    return 0;
  }

  public int hashCode()
  {
    int prime = 31;
    int result = 1;
    result = 31 * result + (this.content == null ? 0 : this.content.hashCode());
    result = 31 * result + (this.dataId == null ? 0 : this.dataId.hashCode());
    result = 31 * result + (this.group == null ? 0 : this.group.hashCode());
    result = 31 * result + (this.md5 == null ? 0 : this.md5.hashCode());
    return result;
  }

  public boolean equals(Object obj)
  {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    ConfigInfo other = (ConfigInfo)obj;
    if (this.content == null) {
      if (other.content != null)
        return false;
    }
    else if (!this.content.equals(other.content))
      return false;
    if (this.dataId == null) {
      if (other.dataId != null)
        return false;
    }
    else if (!this.dataId.equals(other.dataId))
      return false;
    if (this.group == null) {
      if (other.group != null)
        return false;
    }
    else if (!this.group.equals(other.group))
      return false;
    if (this.md5 == null) {
      if (other.md5 != null)
        return false;
    }
    else if (!this.md5.equals(other.md5))
      return false;
    return true;
  }

  public String toString()
  {
    return "[dataId=" + this.dataId + "," + "groupName=" + this.group + "," + "context=" + this.content + "," + "]";
  }
}

