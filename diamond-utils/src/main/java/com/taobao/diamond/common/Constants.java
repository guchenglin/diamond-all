 package com.taobao.diamond.common;

 public class Constants
 {
   public static final String DEFAULT_GROUP = "DEFAULT_GROUP";
   public static final String BASE_DIR = "config-data";
   /**
    *win,linux------
    *  这里配合需要修改本地host    ip a.b.c  其中ip换成你httpserver的ip 一般都和diamond-server 在一起（同一个ip）
    *
    */
   public static final String DEFAULT_DOMAINNAME = "a.b.c";
   //public static final String DEFAULT_DOMAINNAME = "diamond.configserver.net";
   public static final String DAILY_DOMAINNAME = "d.e.f";
   public static final int DEFAULT_PORT = 8080;
   public static final String NULL = "";
   public static final String DATAID = "dataId";
   public static final String GROUP = "group";
   public static final String LAST_MODIFIED = "Last-Modified";
   public static final String ACCEPT_ENCODING = "Accept-Encoding";
   public static final String CONTENT_ENCODING = "Content-Encoding";
   public static final String PROBE_MODIFY_REQUEST = "Probe-Modify-Request";
   public static final String PROBE_MODIFY_RESPONSE = "Probe-Modify-Response";
   public static final String PROBE_MODIFY_RESPONSE_NEW = "Probe-Modify-Response-New";
   public static final String CONTENT_MD5 = "Content-MD5";
   public static final String IF_MODIFIED_SINCE = "If-Modified-Since";
   public static final String SPACING_INTERVAL = "client-spacing-interval";
   public static final int POLLING_INTERVAL_TIME = 15;
   public static final int ONCE_TIMEOUT = 2000;
   public static final int CONN_TIMEOUT = 2000;
   public static final int RECV_WAIT_TIMEOUT = 10000;
   /**
    * //修改加上项目web前缀diamond 这个取决你映射的web名称
    * 如果不加
    *    http://127.0.0.1:8080/config.co?dataId=com.urfresh.scm.common.dynamicConfig.keyIpConfig.localIps&group=SCM
    * 如果加上
    *    http://127.0.0.1:8080/diamond/config.co?dataId=com.urfresh.scm.common.dynamicConfig.keyIpConfig.localIps&group=SCM
    *
    */
   /**
    *  前面一个是项目名，官网给的没有，
    *  部署的时候要映射到没有项目，这里加上项目,
    */
   public static final String HTTP_URI_FILE = "/diamond/config.co";
   /**
    *  前面一个是项目名，官网给的没有，
    *  部署的时候要映射到没有项目，这里加上项目,
    *  否者会拿不到服务器生成的列表，被拦到登陆页面，客户端获取服务列表失败，
    *
    *网上很多教程都把其中，一段代码注释调，不报错，但很少有人知道什么愿意，主要是因为他们没有使用本地缓存。
    */

   public static final String CONFIG_HTTP_URI_FILE = "/diamond/diamond";
   public static final String HTTP_URI_LOGIN = "diamond/url";
   public static final String ENCODE = "GBK";
   public static final String LINE_SEPARATOR = Character.toString('\001');

   public static final String WORD_SEPARATOR = Character.toString('\002');
   public static final String DEFAULT_USERNAME = "xxx";
   public static final String DEFAULT_PASSWORD = "xxx";
   public static final int BATCH_OP_ERROR = -1;
   public static final int BATCH_QUERY_EXISTS = 1;
   public static final int BATCH_QUERY_NONEXISTS = 2;
   public static final int BATCH_ADD_SUCCESS = 3;
   public static final int BATCH_UPDATE_SUCCESS = 4;
 }

