 package com.taobao.diamond.utils;

 import java.util.concurrent.ConcurrentHashMap;
 import java.util.concurrent.ConcurrentMap;

 public class SimpleCache<E>
 {
   private ConcurrentMap<String, CacheEntry<E>> cache;
   private long cacheTTL;

   public SimpleCache()
   {
     this(15000L);
   }

   public SimpleCache(long cacheTTL)
   {
     this.cache = new ConcurrentHashMap();
     this.cacheTTL = cacheTTL;
   }

   public void put(String key, E e)
   {
     if ((key == null) || (e == null)) {
       return;
     }
     CacheEntry entry = new CacheEntry(e, System.currentTimeMillis() + this.cacheTTL);
     this.cache.put(key, entry);
   }

   public E get(String key)
   {
     E result = null;
     CacheEntry<E> entry = (CacheEntry)this.cache.get(key);
     if ((entry != null) &&
       (entry.timestamp > System.currentTimeMillis())) {
       result = entry.value;
     }

     return result;
   }

   private static class CacheEntry<E>
   {
     public final long timestamp;
     public final E value;

     public CacheEntry(E value, long timestamp)
     {
       this.timestamp = timestamp;
       this.value = value;
     }
   }
 }

