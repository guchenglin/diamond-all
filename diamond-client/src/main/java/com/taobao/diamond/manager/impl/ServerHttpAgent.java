package com.taobao.diamond.manager.impl;

import org.apache.commons.httpclient.NameValuePair;

import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.util.Iterator;
import java.util.List;

/**
 * <p></p>
 * User: <a href="mailto:yanmingming1989@163.com">严明明</a>
 * Date: 15/7/23
 * Time: 下午3:59
 */
public class ServerHttpAgent {

    volatile String currentServerIp ;

    public HttpSimpleClient.HttpResult httpGet(String path, List<NameValuePair> paramValues, String encoding, long readTimeoutMs) throws IOException {
        long endTime = System.currentTimeMillis() + readTimeoutMs;
        if (null != this.currentServerIp) {
            try {
                return HttpSimpleClient.httpGet(this.currentServerIp, getUrl(this.currentServerIp, path), paramValues, encoding, readTimeoutMs);
            } catch (ConnectException ce) {
            } catch (SocketTimeoutException stoe) {
            } catch (IOException ioe) {
                throw ioe;
            }
        }

        for (Iterator serverIter = ServerListManager.getApacheServerList().iterator(); serverIter.hasNext(); ) {
            long timeout = endTime - System.currentTimeMillis();
            if (timeout <= 0L) {
                this.currentServerIp = ((String) serverIter.next());
                throw new IOException("timeout");
            }

            String ip = (String) serverIter.next();
            try {
                HttpSimpleClient.HttpResult result = HttpSimpleClient.httpGet(ip, getUrl(ip, path), paramValues, encoding, timeout);
                this.currentServerIp = ip;
                return result;
            } catch (ConnectException ce) {
            } catch (SocketTimeoutException stoe) {
            } catch (IOException ioe) {
                throw ioe;
            }
        }
        throw new ConnectException("no available server");
    }

    public HttpSimpleClient.HttpResult httpPost(String path, List<NameValuePair> paramValues, String encoding, long readTimeoutMs) throws IOException {
        long endTime = System.currentTimeMillis() + readTimeoutMs;

        if (null != this.currentServerIp) {
            try {
                return HttpSimpleClient.httpPost(this.currentServerIp, getUrl(this.currentServerIp, path), paramValues, encoding, readTimeoutMs);
            } catch (ConnectException ce) {
            } catch (SocketTimeoutException stoe) {
            } catch (IOException ioe) {
                throw ioe;
            }
        }

        for (Iterator serverIter = ServerListManager.getApacheServerList().iterator(); serverIter.hasNext(); ) {
            long timeout = endTime - System.currentTimeMillis();
            if (timeout <= 0L) {
                this.currentServerIp = ((String) serverIter.next());
                throw new IOException("timeout");
            }

            String ip = (String) serverIter.next();
            try {
                HttpSimpleClient.HttpResult result = HttpSimpleClient.httpPost(ip, getUrl(ip, path), paramValues, encoding, timeout);

                this.currentServerIp = ip;
                return result;
            } catch (ConnectException ce) {
            } catch (SocketTimeoutException stoe) {
            } catch (IOException ioe) {
                throw ioe;
            }
        }
        throw new ConnectException("no available server");
    }

    static String getUrl(String ip, String relativePath) {
        if (relativePath != null && relativePath.startsWith("/")) {
            return "http://" + ip + ":8080" + relativePath;
        } else {
            return "http://" + ip + ":8080/" + relativePath;
        }

    }


}
