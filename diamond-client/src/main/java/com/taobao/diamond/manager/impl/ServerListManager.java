package com.taobao.diamond.manager.impl;

import com.taobao.diamond.util.DiamondUtil;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>获取Diamond的服务端列表</p>
 * User: <a href="mailto:yanmingming1989@163.com">严明明</a>
 * Date: 15/7/23
 * Time: 下午4:00
 */
public class ServerListManager {
    static volatile List<String> serverIps = new ArrayList();
    static final String DEFAULT_GROUP = "HAITAO";
    static final String DEFAULT_DATEID = "com.sfebiz.common.diamond.serverip";


    public static List<String> getApacheServerList() {
        String serverListContent = DiamondUtil.getAvailableDataIdContent(DEFAULT_GROUP, DEFAULT_DATEID);
        if (StringUtils.isNotBlank(serverListContent)) {
            String[] serverList = serverListContent.split("\r");
            for (String server : serverList) {
                server = server.replace("\r", "");
                server = server.replace("\n", "");
                if (!serverIps.contains(server)) {
                    serverIps.add(server);
                }
            }
        }
        return serverIps;
    }

    public static void main(String[] args) {
        ServerListManager.getApacheServerList();
    }
}
