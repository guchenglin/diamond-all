package com.taobao.diamond.manager.impl;

import com.taobao.diamond.util.DiamondUtil;
import org.apache.commons.lang.StringUtils;

/**
 * <p></p>
 * User: <a href="mailto:yanmingming1989@163.com">严明明</a>
 * Date: 15/7/23
 * Time: 下午4:59
 */
public class ServerLadpConfig {

    /**
     * 默认LDAP的账号姓名
     */
    public static String DEFAULT_LOGIN_DIAMOND_USERNAME;

    /**
     * 默认LDAP的账号密码
     */
    public static String DEFAULT_LOGIN_DIAMOND_PASSWORD;

    static final String DEFAULT_GROUP = "HAITAO";
    static final String DEFAULT_DATEID = "com.sfebiz.common.diamond.ldap";


    static {
        String ladpAccount = DiamondUtil.getAvailableDataIdContent(DEFAULT_GROUP, DEFAULT_DATEID);
        if (StringUtils.isNotBlank(ladpAccount)) {
            String[] ladpAccounts = ladpAccount.split("\r");
            if (ladpAccounts.length > 0) {
                ladpAccount = ladpAccounts[0];
                String[] accountAndPwd = ladpAccount.split("=");
                if(accountAndPwd.length>1){
                    DEFAULT_LOGIN_DIAMOND_USERNAME = accountAndPwd[0].trim();
                    DEFAULT_LOGIN_DIAMOND_PASSWORD = accountAndPwd[1].trim();
                }
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(ServerLadpConfig.DEFAULT_LOGIN_DIAMOND_USERNAME+ServerLadpConfig.DEFAULT_LOGIN_DIAMOND_PASSWORD);
    }
}
