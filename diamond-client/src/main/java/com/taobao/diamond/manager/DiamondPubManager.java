package com.taobao.diamond.manager;

/**
 * <p></p>
 * User: <a href="mailto:yanmingming1989@163.com">严明明</a>
 * Date: 15/7/23
 * Time: 下午3:47
 */
public interface DiamondPubManager {


    /**
     * 更新指定DataId里面的指定行数据，如果存在则更新，如果不存在，则追加
     *
     * @param group
     * @param dataId
     * @param originLineContent 原始内容为null时，表示数据追加
     * @param newLineContent
     * @return
     */
    boolean publish(String group, String dataId,String originLineContent, String newLineContent);

    /**
     * 删除指定DataId里面的指定行数据
     *
     * @param group
     * @param dataId
     * @param content
     * @return
     */
    boolean unPublish(String group, String dataId, String content);

    /**
     * 更新指定DataId里面的所有数据，使用content进行覆盖
     *
     * @param group
     * @param dataId
     * @param content
     * @return
     */
    boolean publishAll(String group, String dataId, String content);



}
