package com.urfresh.logistics.common.config.pos;


import com.urfresh.logistics.common.config.ScmDynamicConfig;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Created by zhaojingyang on 2015/12/28.
 */
public class PosConfig {
    public static final String NO_LIMIT = "NO_LIMIT";
    private static final String CONFIGS = "configs";
    private static final String T0_END_TIME = "t0EndTime";

    /**
     * 获取Ip
     *
     * @return
     */
    public static Map<String, String> getPosConfigs() {
        Map<String,String> configs = ScmDynamicConfig.getPosConfig().getAllPropertiesOnDataId(CONFIGS);
        if (null == configs || configs.size() < 1) {
            throw new RuntimeException("配置项未配置ufConfig");
        }
        return configs;
    }

    /**
     * 获取T0截单时间
     * @return
     */
    public static String getT0EndTimeCfg() {
        return ScmDynamicConfig.getPosConfig().getRule(CONFIGS, T0_END_TIME);
    }


    public static void main(String args[]) {
        Map<String, String> values = getPosConfigs();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            System.out.println(entry.getKey() + "    " + entry.getValue());
        }
    }
}
