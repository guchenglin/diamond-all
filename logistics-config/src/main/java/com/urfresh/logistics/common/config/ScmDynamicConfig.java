package com.urfresh.logistics.common.config;

/**
 * SCM动态配置
 */
public class ScmDynamicConfig extends AbstractRuleConfig<String> {

    private final static String LOGISTICS_DYNAMIC_CONFIG_DATAID = "com.urfresh.scm.common.dynamicConfig";
    private final static String LOGISTICS_DYNAMIC_CONFIG_GROUPID = "SCM";


    private static ScmDynamicConfig keyGeneratorIpConfig = new ScmDynamicConfig("keyIpConfig");
    private static ScmDynamicConfig ufConfig = new ScmDynamicConfig("ufConfig");
    private static ScmDynamicConfig posConfig = new ScmDynamicConfig("posConfig");
    private static ScmDynamicConfig basicInfoConfig = new ScmDynamicConfig("basicInfoConfig");
    private static ScmDynamicConfig pmsConfig = new ScmDynamicConfig("pmsConfig");

    protected ScmDynamicConfig(String subkeyPrefix) {
        super(PropertiesConfigFactory.DIAMOND_CONFIG, LOGISTICS_DYNAMIC_CONFIG_DATAID, LOGISTICS_DYNAMIC_CONFIG_GROUPID, subkeyPrefix, new PropertiesConfig.DynamicValueDecoder<String>() {
            @Override
            public String decode(String value) {
                return value;
            }
        });
    }

    protected ScmDynamicConfig(String dataId, String groupId, String subkeyPrefix) {
        super(PropertiesConfigFactory.DIAMOND_CONFIG, dataId, groupId, subkeyPrefix, new PropertiesConfig.DynamicValueDecoder<String>() {
            @Override
            public String decode(String value) {
                return value;
            }
        });
    }

    /**
     * 获取mock相关配置信息
     *
     * @return
     */
    public static ScmDynamicConfig getKeyGeneratorIpConfig() {
        return keyGeneratorIpConfig;
    }

    public static ScmDynamicConfig getUfConfig() {
        return ufConfig;
    }

    public static ScmDynamicConfig getPosConfig() {
        return posConfig;
    }

    public static ScmDynamicConfig getBasicInfoConfig() {
        return basicInfoConfig;
    }

    public static ScmDynamicConfig getPmsConfig() {
        return pmsConfig;
    }
}
