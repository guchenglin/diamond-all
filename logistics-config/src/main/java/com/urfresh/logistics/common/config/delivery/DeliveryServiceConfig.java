package com.urfresh.logistics.common.config.delivery;


import com.urfresh.logistics.common.config.LogisticsDynamicConfig;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by liyanchun on 2016/4/26.
 */
public class DeliveryServiceConfig {

    private static final String DELIVERY_SERVICE = "deliveryService";

    /**
     * 根据key获取配置
     */
    public static String getConfigByKey(String key) {
        String config = LogisticsDynamicConfig.getLogisticsDynamicService().getRule(DELIVERY_SERVICE, key);
        if (StringUtils.isBlank(config)) {
            throw new RuntimeException("未获取到配置" + key);
        }
        return config;
    }

    public static void main(String[] args){

        System.out.println(getConfigByKey("1"));
    }

}
