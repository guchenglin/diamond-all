package com.urfresh.logistics.common.config.kingdee;

import com.alibaba.fastjson.JSON;
import com.urfresh.logistics.common.config.LogisticsDynamicConfig;
import java.util.Map;

/**
 * Created by liyongbing on 2016/5/4.
 */
public class KingDeeConfig {

    public static Map<String,Object> getKingDeeConfig() {
        String config = LogisticsDynamicConfig.getKingDeeConfig().getRule("config", "kingDee");
        Map<String, Object> configs = null;
        try {
            configs = JSON.parseObject(config);
            if (null == configs || configs.size() < 1) {
                throw new IllegalStateException("金蝶未设置配置项" );
            }
        } catch (Exception e) {
            throw new IllegalStateException("配置项异常" );
        }
        return configs;
    }

    public static void main(String[] args){

        System.out.println(getKingDeeConfig());
    }


}
