package com.urfresh.logistics.common.config;

import org.apache.commons.lang.StringUtils;

public class PropertiesConfigFactory {

    public final static String RESOURCE_CONFIG = "RESOURCE_CONFIG";

    public final static String DIAMOND_CONFIG = "DIAMOND_CONFIG";

    public static PropertiesConfig generatePropertiesConfig(String type, String dataId, String dataIdGroup, PropertiesConfig.DynamicValueDecoder dynamicValueDecoder) {
        if (StringUtils.equalsIgnoreCase(RESOURCE_CONFIG, type)) {
            return new PropertiesResourceConfig(dataId, dataIdGroup, dynamicValueDecoder);
        } else if (StringUtils.equalsIgnoreCase(DIAMOND_CONFIG, type)) {
            return new PropertiesDiamondConfig(dataId, dataIdGroup, dynamicValueDecoder);
        }
        return null;
    }
}
