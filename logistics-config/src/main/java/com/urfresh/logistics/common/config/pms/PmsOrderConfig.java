package com.urfresh.logistics.common.config.pms;

import com.urfresh.logistics.common.config.ScmDynamicConfig;

import org.apache.commons.lang3.StringUtils;

/**
 * Created by liyanchun on 2016/4/26.
 */
public class PmsOrderConfig {

    private static final String PO_AUDIT_RULE = "poAuditConfig";

    /**
     * 根据key获取配置
     */
    public static String getConfigByKey(String key) {
        String config = ScmDynamicConfig.getPmsConfig().getRule(PO_AUDIT_RULE, key);
        if (StringUtils.isBlank(config)) {
            throw new RuntimeException("未获取到配置" + key);
        }
        return config;
    }

    public static void main(String[] args){

        System.out.println(getConfigByKey("1"));
    }

}
