package com.urfresh.logistics.common.config.uf;


import com.urfresh.logistics.common.config.ScmDynamicConfig;

import java.util.Map;

/**
 * Created by zhaojingyang on 2015/12/28.
 */
public class UfConfig {

    private static final String CONFIGS = "configs";
    private static final String SYNC_ORDER_SOURCE = "syncOrderSource";

    public static final String CFG_SYNC_STATUS = "syncStatus";
    public static final String CFG_SYNC_ORDER_SOURCE = "syncOrderSource";

    /**
     * 获取Ip
     *
     * @return
     */
    public static Map<String, String> getUfConfigs() {
        Map<String,String> configs = ScmDynamicConfig.getUfConfig().getAllPropertiesOnDataId(CONFIGS);
        if (null == configs || configs.size() < 1) {
            throw new RuntimeException("配置项未配置ufConfig");
        }
        return configs;
    }

    /**
     * 需要同步到前台的单据
     * @return
     */
    public static String getSyncOrderSource() {
        return ScmDynamicConfig.getUfConfig().getRule(CONFIGS, SYNC_ORDER_SOURCE);
    }

    public static void main(String args[]) {
        Map<String, String> values = getUfConfigs();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            System.out.println(entry.getKey() + "    " + entry.getValue());
        }
        String ss = getSyncOrderSource();
    }
}
