package com.urfresh.logistics.common.config.job.pms;

import com.urfresh.logistics.common.config.LogisticsDynamicConfig;


import java.util.Map;

/**
 * Created by yj on 2016/5/5.
 */
public class AutoCreatFileJobConfig {

    private static final String AUTO_CREATE_FILE_JOB = "autoCreateFileJob";

    public static Map<String, String> queryAutoCreatFileJobConfig() {
        Map<String, String> configs = LogisticsDynamicConfig.getAutoCreateFileConfig().getAllPropertiesOnDataId(AUTO_CREATE_FILE_JOB);
        if (null == configs || configs.size() < 1) {
            throw new RuntimeException("未配置配置项autoCreateFileJob");
        }
        return configs;
    }

    public static void main(String args[]) {
        Map<String, String> configs = queryAutoCreatFileJobConfig();
        for (Map.Entry<String, String> e : configs.entrySet()) {
            System.out.println(e.getKey() + "------" + e.getValue());
        }
    }
}
