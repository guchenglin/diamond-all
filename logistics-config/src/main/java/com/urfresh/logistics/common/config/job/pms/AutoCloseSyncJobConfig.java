package com.urfresh.logistics.common.config.job.pms;

import com.urfresh.logistics.common.config.LogisticsDynamicConfig;


import java.util.Map;

/**
 * Created by yj on 2016/4/15.
 */
public class AutoCloseSyncJobConfig {

    private static final String AUTO_CLOSE_SYNC_JOB = "autoCloseSyncJob";

    public static Map<String, String> queryAutoCloseJobConfig() {
        Map<String, String> configs = LogisticsDynamicConfig.getLogisticsDynamicAutoCloseConfig().getAllPropertiesOnDataId(AUTO_CLOSE_SYNC_JOB);
        if (null == configs || configs.size() < 1) {
            throw new RuntimeException("未配置配置项syncDoJob");
        }
        return configs;
    }

    public static void main(String args[]) {
        Map<String, String> configs = queryAutoCloseJobConfig();
        for (Map.Entry<String, String> e : configs.entrySet()) {
            System.out.println(e.getKey() + "------" + e.getValue());
        }
    }
}
