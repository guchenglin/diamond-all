//package com.urfresh.logistics.common.config;
//
//import redis.clients.jedis.HostAndPort;
//import redis.clients.jedis.JedisCluster;
//
//import java.util.HashSet;
//import java.util.Set;
//
///**
// * jedis管理器
// */
//public class JedisManager{
//
//    /** jedis分布式 */
//    private static JedisCluster jedisCluster = null;
//
//    /** 配置分割器 */
//    private static final String CONFIG_SEPARATE = ",";
//
//    /** 服务器和端口分割器 */
//    private static final String HOSTANDPORT_SEPARATE = ":";
//
//    /**
//     * 获取jedisCluster客户端
//     *
//     * @return 获取结果
//     */
//    public static JedisCluster getJedisCluster() {
//        if(jedisCluster==null){
//            synchronized(JedisManager.class){
//                if(jedisCluster==null){
//                    Set<HostAndPort> jedisClusterNodes = new HashSet<HostAndPort>();
//                    String[] configs = null; //= OrderSplit.getRedisConfig().split(CONFIG_SEPARATE);
//                    for (String hostAndPort : configs) {
//                        String[] hostAndPorts = hostAndPort.split(HOSTANDPORT_SEPARATE);
//                        if (hostAndPorts.length < 2) {
//                            continue;
//                        }
//                        String host = hostAndPorts[0];
//                        String port = hostAndPorts[1];
//                        jedisClusterNodes.add(new HostAndPort(host, Integer.parseInt(port)));
//                    }
//                    jedisCluster = new JedisCluster(jedisClusterNodes);
//
//                    if (null == jedisCluster) {
//                        throw new RuntimeException("实例化redis失败！！!");
//                    }
//                }
//            }
//        }
//        return jedisCluster;
//    }
//}
