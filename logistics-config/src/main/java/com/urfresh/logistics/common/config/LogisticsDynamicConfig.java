package com.urfresh.logistics.common.config;

/**
 * SCM动态配置
 */
public class LogisticsDynamicConfig extends AbstractRuleConfig<String> {

    private final static String LOGISTICS_DYNAMIC_CONFIG_DATAID = "com.urfresh.logistics.common.dynamicConfig";
    private final static String LOGISTICS_DYNAMIC_CONFIG_GROUPID = "SCM_LOGISTICS";


    private static LogisticsDynamicConfig logisticsDynamicMockConfig = new LogisticsDynamicConfig("mock");
    private static LogisticsDynamicConfig logisticsDynamicService = new LogisticsDynamicConfig("service");
    private static LogisticsDynamicConfig logisticsDynamicAutoCloseConfig = new LogisticsDynamicConfig("autoCloseConfig");
    private static LogisticsDynamicConfig kingDeeConfig = new LogisticsDynamicConfig("kingDee");
    private static LogisticsDynamicConfig autoCreateFileConfig = new LogisticsDynamicConfig("autoCreateFileConfig");


    protected LogisticsDynamicConfig(String subkeyPrefix) {
        super(PropertiesConfigFactory.DIAMOND_CONFIG, LOGISTICS_DYNAMIC_CONFIG_DATAID, LOGISTICS_DYNAMIC_CONFIG_GROUPID, subkeyPrefix, new PropertiesConfig.DynamicValueDecoder<String>() {
            @Override
            public String decode(String value) {
                return value;
            }
        });
    }

    protected LogisticsDynamicConfig(String dataId,String groupId,String subkeyPrefix) {
        super(PropertiesConfigFactory.DIAMOND_CONFIG, dataId, groupId, subkeyPrefix, new PropertiesConfig.DynamicValueDecoder<String>() {
            @Override
            public String decode(String value) {
                return value;
            }
        });
    }

    public static LogisticsDynamicConfig getLogisticsDynamicAutoCloseConfig() {
        return logisticsDynamicAutoCloseConfig;
    }

    /**
     * 获取mock相关配置信息
     *
     * @return
     */
    public static LogisticsDynamicConfig getMock() {
        return logisticsDynamicMockConfig;
    }

    public static LogisticsDynamicConfig getLogisticsDynamicService() {
        return logisticsDynamicService;
    }

    public static LogisticsDynamicConfig getKingDeeConfig() {
        return kingDeeConfig;
    }

    public static LogisticsDynamicConfig getAutoCreateFileConfig() {
        return autoCreateFileConfig;
    }
}
