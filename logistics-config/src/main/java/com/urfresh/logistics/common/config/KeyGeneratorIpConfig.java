package com.urfresh.logistics.common.config;

import java.util.Map;

/**
 * Created by zhaojingyang on 2015/11/26.
 */
public class KeyGeneratorIpConfig {
    private static final String LOCAL_IPS = "localIps";

    /**
     * 获取Ip
     *
     * @return
     */
    public static Map<String, String> getIpConfigs() {
        return ScmDynamicConfig.getKeyGeneratorIpConfig().getAllPropertiesOnDataId(LOCAL_IPS);
    }

    public static void main(String args[]) {
        Map<String, String> values = getIpConfigs();
        for (Map.Entry<String, String> entry : values.entrySet()) {
            System.out.println(entry.getKey() + "    " + entry.getValue());
        }
    }
}
