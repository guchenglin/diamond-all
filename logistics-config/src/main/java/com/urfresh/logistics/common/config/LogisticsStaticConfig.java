package com.urfresh.logistics.common.config;

/**
 * Created by yj on 2016/5/16.
 */
public class LogisticsStaticConfig {
    public static String poMailPath;

    public static String getPoMailPath() {
        return poMailPath;
    }
    public  void setPoMailPath(String poMailPath) {
        LogisticsStaticConfig.poMailPath = poMailPath;
    }
}
