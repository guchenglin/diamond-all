package com.urfresh.logistics.common.config.basic;


import com.urfresh.logistics.common.config.ScmDynamicConfig;
import org.apache.commons.lang3.StringUtils;

/**
 * 基础信息同步配置
 * Created by zhaojingyang on 2016/4/13.
 */
public class BasicInfoSyncConfig {
    public static final String SYNC_CONFIG = "syncConfig";
    public static final String SYNC_ITEMS = "syncItems";
    public static final String SYNC_SUPPLIER = "syncSupplier";
    public static final String SKU = "sku";
    public static final String SUPPLIER = "supplier";
    private static final String UNDO_IP = "undoIp";
    private static final String SKU_UNDO_IP = "skuUndoIp";
    private static final String FBRNO = "fbrNo";
    private static final String WSDLURL = "wsdlUrl";

    /**
     * 获取商品信息同步配置
     */
    public static String getSkuSyncConfig() {
        return ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, SKU);
    }
    
    public static String getUndoIpConfig(){
        String undoIp = ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, SKU_UNDO_IP);
        if(StringUtils.isEmpty(undoIp)){
            throw new RuntimeException("未配置过滤IP");
        }
        return undoIp;
    }

    public static String getSkuUndoIpConfig(){
        String undoIp = ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, UNDO_IP);
        if(StringUtils.isEmpty(undoIp)){
            throw new RuntimeException("未配置过滤skuUndoIp IP");
        }
        return undoIp;
    }

    /**
     * 根据目标系统获取需要同步字段
     * @param key
     * @return
     */
    public static String getSyncItems(String key) {
        return ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_ITEMS, key);
    }

    public static String getSupplierConfig() {
        return ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, SUPPLIER);
    }

    public static String getFbrNoConfig() {
        return ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, FBRNO);
    }

    public static String getWsdlurl() {
        return  ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, WSDLURL);
    }
    public static String getSyncConfigByKey(String key) {
        return  ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_CONFIG, key);
    }

    public static String getSyncSupplier(String key) {
        return ScmDynamicConfig.getBasicInfoConfig().getRule(SYNC_SUPPLIER,key);
    }
}
