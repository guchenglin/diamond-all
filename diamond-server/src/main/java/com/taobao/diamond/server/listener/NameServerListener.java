//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.listener;

import com.taobao.diamond.utils.ResourceUtils;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;

public class NameServerListener implements ServletContextListener {
    public NameServerListener() {
    }

    public void contextInitialized(ServletContextEvent servletContextEvent) {
        FileOutputStream fileOutputStream = null;

        try {
            Properties props = ResourceUtils.getResourceAsProperties("config.properties");
            if(props != null && StringUtils.isNotBlank(props.getProperty("host"))) {
                String filePath = servletContextEvent.getServletContext().getRealPath("/");
                File file = new File(filePath + "/diamond");
                if(!file.exists()) {
                    file.createNewFile();
                    fileOutputStream = new FileOutputStream(file);
                    IOUtils.write(props.getProperty("host"), fileOutputStream);
                }
            }
        } catch (IOException var14) {
            var14.printStackTrace();
        } finally {
            if(fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException var13) {
                    var13.printStackTrace();
                }
            }

        }

    }

    public void contextDestroyed(ServletContextEvent servletContextEvent) {
    }
}
