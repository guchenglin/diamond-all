//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.domain.ConfigInfo;
import com.taobao.diamond.server.exception.ConfigServiceException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;
import javax.servlet.ServletContext;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.util.WebUtils;

@Service
public class DiskService {
    private static final Log log = LogFactory.getLog(DiskService.class);
    private final ConcurrentHashMap<String, Boolean> modifyMarkCache = new ConcurrentHashMap();
    @Autowired
    private ServletContext servletContext;

    public DiskService() {
    }

    public void setServletContext(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public ServletContext getServletContext() {
        return this.servletContext;
    }

    public ConcurrentHashMap<String, Boolean> getModifyMarkCache() {
        return this.modifyMarkCache;
    }

    public String getFilePath(String dataId, String group) throws FileNotFoundException {
        return this.getFilePath("config-data/" + group + "/" + dataId);
    }

    public void saveToDisk(ConfigInfo configInfo) {
        String group = configInfo.getGroup();
        String dataId = configInfo.getDataId();
        String content = configInfo.getContent();
        String cacheKey = this.generateCacheKey(group, dataId);
        if(this.modifyMarkCache.putIfAbsent(cacheKey, Boolean.valueOf(true)) == null) {
            File tempFile = null;

            try {
                String groupPath = this.getFilePath("config-data/" + group);
                this.createDirIfNessary(groupPath);
                File targetFile = this.createFileIfNessary(groupPath, dataId);
                tempFile = this.createTempFile(dataId, group);
                FileUtils.writeStringToFile(tempFile, content, "GBK");
                FileUtils.copyFile(tempFile, targetFile);
            } catch (Exception var12) {
                String errorMsg = "save disk error, dataId=" + dataId + ",group=" + group;
                log.error(errorMsg, var12);
                throw new ConfigServiceException(errorMsg, var12);
            } finally {
                if(tempFile != null && tempFile.exists()) {
                    FileUtils.deleteQuietly(tempFile);
                }

                this.modifyMarkCache.remove(cacheKey);
            }

        } else {
            throw new ConfigServiceException("config info is being motified, dataId=" + dataId + ",group=" + group);
        }
    }

    public boolean isModified(String dataId, String group) {
        return this.modifyMarkCache.get(this.generateCacheKey(group, dataId)) != null;
    }

    public final String generateCacheKey(String group, String dataId) {
        return group + "/" + dataId;
    }

    public void removeConfigInfo(String dataId, String group) {
        String cacheKey = this.generateCacheKey(group, dataId);
        if(this.modifyMarkCache.putIfAbsent(cacheKey, Boolean.valueOf(true)) != null) {
            throw new ConfigServiceException("config info is being motified, dataId=" + dataId + ",group=" + group);
        } else {
            try {
                String errorMsg;
                try {
                    String basePath = this.getFilePath("config-data");
                    this.createDirIfNessary(basePath);
                    errorMsg = this.getFilePath("config-data/" + group);
                    File groupDir = new File(errorMsg);
                    if(!groupDir.exists()) {
                        return;
                    }

                    String dataPath = this.getFilePath("config-data/" + group + "/" + dataId);
                    File dataFile = new File(dataPath);
                    if(dataFile.exists()) {
                        FileUtils.deleteQuietly(dataFile);
                        return;
                    }
                } catch (Exception var12) {
                    errorMsg = "delete config info error, dataId=" + dataId + ",group=" + group;
                    log.error(errorMsg, var12);
                    throw new ConfigServiceException(errorMsg, var12);
                }
            } finally {
                this.modifyMarkCache.remove(cacheKey);
            }

        }
    }

    private String getFilePath(String dir) throws FileNotFoundException {
        return WebUtils.getRealPath(this.servletContext, dir);
    }

    private void createDirIfNessary(String path) {
        File dir = new File(path);
        if(!dir.exists()) {
            dir.mkdirs();
        }

    }

    private File createFileIfNessary(String parent, String child) throws IOException {
        File file = new File(parent, child);
        if(!file.exists()) {
            file.createNewFile();
            this.changeFilePermission(file);
        }

        return file;
    }

    private void changeFilePermission(File file) {
        file.setExecutable(false, false);
        file.setWritable(false, false);
        file.setReadable(false, false);
        file.setExecutable(false, true);
        file.setWritable(true, true);
        file.setReadable(true, true);
    }

    private File createTempFile(String dataId, String group) throws IOException {
        return File.createTempFile(group + "-" + dataId, ".tmp");
    }
}
