//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.utils;

import com.taobao.diamond.server.utils.SessionHolder.*;
import javax.servlet.http.HttpSession;

public class SessionHolder {
    private static ThreadLocal<HttpSession> sessionThreadLocal = new ThreadLocal();

    public SessionHolder() {
    }

    public static void invalidate() {
        sessionThreadLocal.remove();
    }

    public static void setSession(HttpSession session) {
        sessionThreadLocal.set(session);
    }

    public static HttpSession getSession() {
        return (HttpSession)sessionThreadLocal.get();
    }
}
