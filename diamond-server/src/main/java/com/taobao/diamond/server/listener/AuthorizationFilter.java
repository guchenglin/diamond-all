//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.listener;

import com.taobao.diamond.server.utils.SessionHolder;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class AuthorizationFilter implements Filter {
    public AuthorizationFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest)request;
        HttpSession session = httpRequest.getSession();
        SessionHolder.setSession(session);

        try {
            if(session.getAttribute("user") == null) {
                ((HttpServletResponse)response).sendRedirect(httpRequest.getContextPath() + "/jsp/login.jsp");
            } else {
                chain.doFilter(httpRequest, response);
            }
        } finally {
            SessionHolder.invalidate();
        }

    }

    public void init(FilterConfig filterConfig) throws ServletException {
    }
}
