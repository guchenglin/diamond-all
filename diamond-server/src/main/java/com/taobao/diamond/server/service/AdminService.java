//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.utils.ResourceUtils;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;

@Service
public class AdminService {
    private static final Log log = LogFactory.getLog(AdminService.class);
    private volatile Properties properties = new Properties();
    private URL url;

    public URL getUrl() {
        return this.url;
    }

    public AdminService() {
        this.loadUsers();
    }

    public void loadUsers() {
        Properties tempProperties = new Properties();
        FileInputStream in = null;

        try {
            this.url = ResourceUtils.getResourceURL("user.properties");
            in = new FileInputStream(this.url.getPath());
            tempProperties.load(in);
        } catch (IOException var12) {
            log.error("加载user.properties文件失败", var12);
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException var11) {
                    log.error("关闭user.properties文件失败", var11);
                }
            }

        }

        this.properties = tempProperties;
    }

    public synchronized boolean login(String userName, String password) {
        String passwordInFile = this.properties.getProperty(userName);
        return passwordInFile != null?passwordInFile.equals(password):false;
    }

    public synchronized boolean addUser(String userName, String password) {
        return this.properties.containsKey(userName)?false:this.saveToDisk();
    }

    private boolean saveToDisk() {
        FileOutputStream out = null;

        boolean var3;
        try {
            out = new FileOutputStream(this.url.getPath());
            this.properties.store(out, "add user");
            out.flush();
            boolean var2 = true;
            return var2;
        } catch (IOException var13) {
            log.error("保存user.properties文件失败", var13);
            var3 = false;
        } finally {
            if(out != null) {
                try {
                    out.close();
                } catch (IOException var12) {
                    log.error("关闭user.properties文件失败", var12);
                }
            }

        }

        return var3;
    }

    public synchronized Map<String, String> getAllUsers() {
        Map<String, String> result = new HashMap();
        Enumeration enu = this.properties.keys();

        while(enu.hasMoreElements()) {
            String address = (String)enu.nextElement();
            String group = this.properties.getProperty(address);
            result.put(address, group);
        }

        return result;
    }

    public synchronized boolean updatePassword(String userName, String newPassword) {
        return !this.properties.containsKey(userName)?false:this.saveToDisk();
    }

    public synchronized boolean removeUser(String userName) {
        return this.properties.size() == 1?false:(!this.properties.containsKey(userName)?false:this.saveToDisk());
    }
}
