//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.domain.ConfigInfo;
import com.taobao.diamond.domain.Page;
import java.io.IOException;
import java.util.Iterator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public final class DumpConfigInfoTask implements Runnable {
    private static final Log log = LogFactory.getLog(DumpConfigInfoTask.class);
    private static final int PAGE_SIZE = 1000;
    private final TimerTaskService timerTaskService;

    public DumpConfigInfoTask(TimerTaskService timerTaskService) {
        this.timerTaskService = timerTaskService;
    }

    public void run() {
        try {
            Page<ConfigInfo> page = this.timerTaskService.getPersistService().findAllConfigInfo(1, 1000);
            if(page != null) {
                int totalPages = page.getPagesAvailable();
                this.updateConfigInfo(page);
                if(totalPages > 1) {
                    for(int pageNo = 2; pageNo <= totalPages; ++pageNo) {
                        page = this.timerTaskService.getPersistService().findAllConfigInfo(pageNo, 1000);
                        if(page != null) {
                            this.updateConfigInfo(page);
                        }
                    }
                }
            }
        } catch (Throwable var4) {
            log.error("dump task run error", var4);
        }

    }

    private void updateConfigInfo(Page<ConfigInfo> page) throws IOException {
        Iterator i$ = page.getPageItems().iterator();

        while(i$.hasNext()) {
            ConfigInfo configInfo = (ConfigInfo)i$.next();
            if(configInfo != null) {
                try {
                    this.timerTaskService.getConfigService().updateMD5Cache(configInfo);
                    this.timerTaskService.getDiskService().saveToDisk(configInfo);
                } catch (Throwable var5) {
                    log.error("dump config info error, dataId=" + configInfo.getDataId() + ", group=" + configInfo.getGroup(), var5);
                }
            }
        }

    }
}
