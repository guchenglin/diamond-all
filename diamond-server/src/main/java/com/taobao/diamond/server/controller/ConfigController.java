package com.taobao.diamond.server.controller;

import com.taobao.diamond.common.Constants;
import com.taobao.diamond.server.service.ConfigService;
import com.taobao.diamond.server.service.DiskService;
import com.taobao.diamond.server.utils.GlobalCounter;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ConfigController {
    @Autowired
    private ConfigService configService;
    @Autowired
    private DiskService diskService;

    public ConfigController() {
    }

    public String getConfig(HttpServletRequest request, HttpServletResponse response, String dataId, String group) {
        response.setHeader("Content-Type", "text/html;charset=gbk");
        String address = this.getRemortIP(request);
        if(address == null) {
            response.setStatus(400);
            return "400";
        } else if(GlobalCounter.getCounter().decrementAndGet() >= 0L) {
            response.setStatus(503);
            return "503";
        } else {
            String md5 = this.configService.getContentMD5(dataId, group);
            if(md5 == null) {
                return "404";
            } else {
                response.setHeader("Content-MD5", md5);
                if(this.diskService.isModified(dataId, group)) {
                    response.setStatus(304);
                    return "304";
                } else {
                    String path = this.configService.getConfigInfoPath(dataId, group);
                    if(this.diskService.isModified(dataId, group)) {
                        response.setStatus(304);
                        return "304";
                    } else {
                        response.setHeader("Pragma", "no-cache");
                        response.setDateHeader("Expires", 0L);
                        response.setHeader("Cache-Control", "no-cache,no-store");
                        return "forward:" + path;
                    }
                }
            }
        }
    }

    public String getProbeModifyResult(HttpServletRequest request, HttpServletResponse response, String probeModify) {
        response.setHeader("Content-Type", "text/html;charset=gbk");
        String address = this.getRemortIP(request);
        if(address == null) {
            response.setStatus(400);
            return "400";
        } else if(GlobalCounter.getCounter().decrementAndGet() >= 0L) {
            response.setStatus(503);
            return "503";
        } else {
            List<ConfigKey> configKeyList = getConfigKeyList(probeModify);
            StringBuilder resultBuilder = new StringBuilder();
            Iterator i$ = configKeyList.iterator();

            while(i$.hasNext()) {
                ConfigKey key = (ConfigKey)i$.next();
                String md5 = this.configService.getContentMD5(key.getDataId(), key.getGroup());
                if(!StringUtils.equals(md5, key.getMd5())) {
                    resultBuilder.append(key.getDataId()).append(Constants.WORD_SEPARATOR).append(key.getGroup()).append(Constants.LINE_SEPARATOR);
                }
            }

            String returnHeader = resultBuilder.toString();

            try {
                returnHeader = URLEncoder.encode(resultBuilder.toString(), "gbk");
            } catch (Exception var10) {
                var10.printStackTrace();
            }

            request.setAttribute("content", returnHeader);
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0L);
            response.setHeader("Cache-Control", "no-cache,no-store");
            return "200";
        }
    }

    public ConfigService getConfigService() {
        return this.configService;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public DiskService getDiskService() {
        return this.diskService;
    }

    public void setDiskService(DiskService diskService) {
        this.diskService = diskService;
    }

    public String getRemortIP(HttpServletRequest request) {
        return request.getHeader("x-forwarded-for") == null?request.getRemoteAddr():request.getHeader("x-forwarded-for");
    }

    public static List<ConfigKey> getConfigKeyList(String configKeysString) {
        List<ConfigKey> configKeyList = new LinkedList();
        if(null != configKeysString && !"".equals(configKeysString)) {
            String[] configKeyStrings = configKeysString.split(Constants.LINE_SEPARATOR);
            String[] arr$ = configKeyStrings;
            int len$ = configKeyStrings.length;

            for(int i$ = 0; i$ < len$; ++i$) {
                String configKeyString = arr$[i$];
                String[] configKey = configKeyString.split(Constants.WORD_SEPARATOR);
                if(configKey.length <= 3) {
                    ConfigKey key = new ConfigKey();
                    if(!"".equals(configKey[0])) {
                        key.setDataId(configKey[0]);
                        if(configKey.length >= 2 && !"".equals(configKey[1])) {
                            key.setGroup(configKey[1]);
                        }

                        if(configKey.length == 3 && !"".equals(configKey[2])) {
                            key.setMd5(configKey[2]);
                        }

                        configKeyList.add(key);
                    }
                }
            }

            return configKeyList;
        } else {
            return configKeyList;
        }
    }

    public static class ConfigKey {
        private String dataId;
        private String group;
        private String md5;

        public String getDataId() {
            return this.dataId;
        }

        public void setDataId(String dataId)
        {
            this.dataId = dataId;
        }

        public String getGroup()
        {
            return this.group;
        }

        public void setGroup(String group)
        {
            this.group = group;
        }

        public String getMd5()
        {
            return this.md5;
        }

        public void setMd5(String md5)
        {
            this.md5 = md5;
        }

        public String toString()
        {
            StringBuilder sb = new StringBuilder();
            sb.append("DataID: ").append(this.dataId).append("\r\n");
            sb.append("Group: ").append(null == this.group ? "" : this.group).append("\r\n");
            sb.append("MD5: ").append(null == this.md5 ? "" : this.md5).append("\r\n");
            return sb.toString();
        }
    }
}

