//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.exception;

public class ConfigServiceException extends RuntimeException {
  static final long serialVersionUID = -1L;

  public ConfigServiceException() {
  }

  public ConfigServiceException(String message, Throwable cause) {
    super(message, cause);
  }

  public ConfigServiceException(String message) {
    super(message);
  }

  public ConfigServiceException(Throwable cause) {
    super(cause);
  }
}
