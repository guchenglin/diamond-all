//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.domain.ConfigInfo;
import com.taobao.diamond.domain.Page;
import com.taobao.diamond.md5.MD5;
import com.taobao.diamond.server.exception.ConfigServiceException;
import java.util.concurrent.ConcurrentHashMap;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class ConfigService {
    private static final Log log = LogFactory.getLog(ConfigService.class);
    @Autowired
    private PersistService persistService;
    @Autowired
    private DiskService diskService;
    @Autowired
    private NotifyService notifyService;
    private final ConcurrentHashMap<String, String> contentMD5Cache = new ConcurrentHashMap();

    public ConfigService() {
    }

    public ConfigInfo getConfigInfoById(long id) {
        ConfigInfo configInfo = this.persistService.findConfigInfo(id);
        return configInfo;
    }

    public String getConfigInfoPath(String dataId, String group) {
        StringBuilder sb = new StringBuilder("/");
        sb.append("config-data").append("/");
        sb.append(group).append("/");
        sb.append(dataId);
        return sb.toString();
    }

    public void updateMD5Cache(ConfigInfo configInfo) {
        this.contentMD5Cache.put(this.generateMD5CacheKey(configInfo.getDataId(), configInfo.getGroup()), MD5.getInstance().getMD5String(configInfo.getContent()));
    }

    public String getContentMD5(String dataId, String group) {
        String key = this.generateMD5CacheKey(dataId, group);
        String md5 = (String)this.contentMD5Cache.get(key);
        if(md5 == null) {
            synchronized(this) {
                return (String)this.contentMD5Cache.get(key);
            }
        } else {
            return md5;
        }
    }

    String generateMD5CacheKey(String dataId, String group) {
        String key = group + "/" + dataId;
        return key;
    }

    String generatePath(String dataId, String group) {
        StringBuilder sb = new StringBuilder("/");
        sb.append("config-data").append("/");
        sb.append(group).append("/");
        sb.append(dataId);
        return sb.toString();
    }

    public void removeConfigInfo(long id) {
        try {
            ConfigInfo configInfo = this.persistService.findConfigInfo(id);
            this.diskService.removeConfigInfo(configInfo.getDataId(), configInfo.getGroup());
            this.contentMD5Cache.remove(this.generateMD5CacheKey(configInfo.getDataId(), configInfo.getGroup()));
            this.persistService.removeConfigInfo(configInfo);
            this.notifyOtherNodes(configInfo.getDataId(), configInfo.getGroup());
        } catch (Exception var4) {
            log.error("删除配置信息错误", var4);
            throw new ConfigServiceException(var4);
        }
    }

    public void addConfigInfo(String dataId, String group, String content) {
        this.checkParameter(dataId, group, content);
        ConfigInfo configInfo = new ConfigInfo(dataId, group, content);

        try {
            this.persistService.addConfigInfo(configInfo);
            this.contentMD5Cache.put(this.generateMD5CacheKey(dataId, group), configInfo.getMd5());
            this.diskService.saveToDisk(configInfo);
            this.notifyOtherNodes(dataId, group);
        } catch (Exception var6) {
            log.error("保存ConfigInfo失败", var6);
            throw new ConfigServiceException(var6);
        }
    }

    public void updateConfigInfo(String dataId, String group, String content) {
        this.checkParameter(dataId, group, content);
        ConfigInfo configInfo = new ConfigInfo(dataId, group, content);

        try {
            this.persistService.updateConfigInfo(configInfo);
            this.contentMD5Cache.put(this.generateMD5CacheKey(dataId, group), configInfo.getMd5());
            this.diskService.saveToDisk(configInfo);
            this.notifyOtherNodes(dataId, group);
        } catch (Exception var6) {
            log.error("保存ConfigInfo失败", var6);
            throw new ConfigServiceException(var6);
        }
    }

    public void loadConfigInfoToDisk(String dataId, String group) {
        try {
            ConfigInfo configInfo = this.persistService.findConfigInfo(dataId, group);
            if(configInfo != null) {
                this.contentMD5Cache.put(this.generateMD5CacheKey(dataId, group), configInfo.getMd5());
                this.diskService.saveToDisk(configInfo);
            } else {
                this.contentMD5Cache.remove(this.generateMD5CacheKey(dataId, group));
                this.diskService.removeConfigInfo(dataId, group);
            }

        } catch (Exception var4) {
            log.error("保存ConfigInfo到磁盘失败", var4);
            throw new ConfigServiceException(var4);
        }
    }

    public ConfigInfo findConfigInfo(String dataId, String group) {
        return this.persistService.findConfigInfo(dataId, group);
    }

    public Page<ConfigInfo> findConfigInfo(int pageNo, int pageSize, String group, String dataId) {
        if(StringUtils.hasLength(dataId) && StringUtils.hasLength(group)) {
            ConfigInfo ConfigInfo = this.persistService.findConfigInfo(dataId, group);
            Page<ConfigInfo> page = new Page();
            if(ConfigInfo != null) {
                page.setPageNumber(1);
                page.setTotalCount(1);
                page.setPagesAvailable(1);
                page.getPageItems().add(ConfigInfo);
            }

            return page;
        } else {
            return StringUtils.hasLength(dataId) && !StringUtils.hasLength(group)?this.persistService.findConfigInfoByDataId(pageNo, pageSize, dataId):(!StringUtils.hasLength(dataId) && StringUtils.hasLength(group)?this.persistService.findConfigInfoByGroup(pageNo, pageSize, group):this.persistService.findAllConfigInfo(pageNo, pageSize));
        }
    }

    public Page<ConfigInfo> findConfigInfoLike(int pageNo, int pageSize, String group, String dataId) {
        return this.persistService.findConfigInfoLike(pageNo, pageSize, dataId, group);
    }

    private void checkParameter(String dataId, String group, String content) {
        if(StringUtils.hasLength(dataId) && !StringUtils.containsWhitespace(dataId)) {
            if(StringUtils.hasLength(group) && !StringUtils.containsWhitespace(group)) {
                if(!StringUtils.hasLength(content)) {
                    throw new ConfigServiceException("无效的content");
                }
            } else {
                throw new ConfigServiceException("无效的group");
            }
        } else {
            throw new ConfigServiceException("无效的dataId");
        }
    }

    private void notifyOtherNodes(String dataId, String group) {
        this.notifyService.notifyConfigInfoChange(dataId, group);
    }

    public DiskService getDiskService() {
        return this.diskService;
    }

    public void setDiskService(DiskService diskService) {
        this.diskService = diskService;
    }

    public PersistService getPersistService() {
        return this.persistService;
    }

    public void setPersistService(PersistService persistService) {
        this.persistService = persistService;
    }

    public NotifyService getNotifyService() {
        return this.notifyService;
    }

    public void setNotifyService(NotifyService notifyService) {
        this.notifyService = notifyService;
    }
}
