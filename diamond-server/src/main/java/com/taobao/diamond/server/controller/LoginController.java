//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.controller;

import com.taobao.diamond.server.service.AdminService;
import com.taobao.diamond.server.service.UserDaoLdapImpl;
import com.taobao.diamond.server.utils.MD5Helper;
import java.util.Map;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping({"/login.do"})
public class LoginController {
    private static final Log log = LogFactory.getLog(LoginController.class);
    @Autowired
    private AdminService adminService;
    @Resource
    UserDaoLdapImpl userDaoLdap;

    public LoginController() {
    }

    @RequestMapping(
            params = {"method=login"},
            method = {RequestMethod.POST}
    )
    public String login(HttpServletRequest request, @RequestParam("username") String username, @RequestParam("password") String password, ModelMap modelMap) {
        boolean flag = false;

        try {
            Map<String, String> allUsers = this.adminService.getAllUsers();
            if(StringUtils.isNotBlank(username) && StringUtils.isNotBlank(password)) {
                if(username.equals("guchenglin") || username.equals("resto")) {
                    String passwords = (String)allUsers.get(username);
                    String secret = MD5Helper.MD5(password);
                    flag = passwords.equals(secret);
                    if(flag) {
                        request.getSession().setAttribute("user", username);
                        return "admin/admin";
                    }
                }

                if(this.userDaoLdap.userLogin(username, password)) {
                    request.getSession().setAttribute("user", username);
                    return "admin/admin";
                } else {
                    modelMap.addAttribute("message", "登录失败，用户名密码不匹配,请使用ldap的用户名密码登陆");
                    return "login";
                }
            } else {
                modelMap.addAttribute("message", "登录失败，用户名密码不匹配");
                return "login";
            }
        } catch (Exception var9) {
            log.error(var9);
            modelMap.addAttribute("message", "ldap链接挂了~~请联系管理员");
            return "login";
        }
    }

    public AdminService getAdminService() {
        return this.adminService;
    }

    public void setAdminService(AdminService adminService) {
        this.adminService = adminService;
    }

    @RequestMapping(
            params = {"method=logout"},
            method = {RequestMethod.GET}
    )
    public String logout(HttpServletRequest request) {
        request.getSession().invalidate();
        return "login";
    }
}
