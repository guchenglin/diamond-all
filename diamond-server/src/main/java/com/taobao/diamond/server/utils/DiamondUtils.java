 package com.taobao.diamond.server.utils;

 import java.sql.Timestamp;
 import java.util.Date;

 public class DiamondUtils
 {
  static final char[] INVALID_CHAR = { ';', '&', '%', '#', '$', '@', ',', '*', '^', '~', '(', ')', '/', '\\', '|', '+' };

   public static boolean hasInvalidChar(String str)
   {
    if ((str == null) || (str.length() == 0))
       return true;
     int strLen = str.length();
     for (int i = 0; i < strLen; i++) {
       char ch = str.charAt(i);
      if ((Character.isWhitespace(ch)) || (isInvalidChar(ch))) {
         return true;
       }
     }
    return false;
   }

   public static boolean isInvalidChar(char ch)
   {
     for (char c : INVALID_CHAR) {
       if (c == ch)
       return true;
     }
    return false;
   }

   public static Timestamp getCurrentTime()
   {
    Date date = new Date();
    return new Timestamp(date.getTime());
   }
 }
