//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.server.service.TimerTaskService.*;
import com.taobao.diamond.server.utils.SystemConfig;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TimerTaskService {
    private static final String THREAD_NAME = "diamond dump config thread";
    @Autowired
    private PersistService persistService;
    @Autowired
    private DiskService diskService;
    @Autowired
    private ConfigService configService;
    private ScheduledExecutorService scheduledExecutorService;

    public TimerTaskService() {
    }

    @PostConstruct
    public void init() {
        this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor(new ThreadFactory()
        {
            public Thread newThread(Runnable r) {
                Thread t = new Thread(r);
                t.setName("diamond dump config thread");
                t.setDaemon(true);
                return t;
            }
        });;
        DumpConfigInfoTask dumpTask = new DumpConfigInfoTask(this);
        dumpTask.run();
        this.scheduledExecutorService.scheduleWithFixedDelay(dumpTask, (long)SystemConfig.getDumpConfigInterval(), (long)SystemConfig.getDumpConfigInterval(), TimeUnit.SECONDS);
    }

    @PreDestroy
    public void dispose() {
        if(this.scheduledExecutorService != null) {
            this.scheduledExecutorService.shutdown();
        }

    }

    public void setPersistService(PersistService persistService) {
        this.persistService = persistService;
    }

    public PersistService getPersistService() {
        return this.persistService;
    }

    public void setDiskService(DiskService diskService) {
        this.diskService = diskService;
    }

    public ConfigService getConfigService() {
        return this.configService;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    public DiskService getDiskService() {
        return this.diskService;
    }
}
