//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

public enum OperatorEnum {
    ADD,
    DELETE,
    UPDATE,
    BATCHADDORUPDATE;

    private OperatorEnum() {
    }
}
