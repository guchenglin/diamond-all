//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.service;

import com.taobao.diamond.server.utils.SystemConfig;
import com.taobao.diamond.utils.ResourceUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
public class NotifyService {
    private static final int TIMEOUT = 5000;
    private final String URL_PREFIX = "/diamond-server/notify.do";
    private final String PROTOCOL = "http://";
    private final Properties nodeProperties = new Properties();
    static final Log log = LogFactory.getLog(NotifyService.class);

    public NotifyService() {
    }

    Properties getNodeProperties() {
        return this.nodeProperties;
    }

    @PostConstruct
    public void loadNodes() {
        InputStream in = null;

        try {
            in = ResourceUtils.getResourceAsStream("node.properties");
            this.nodeProperties.load(in);
        } catch (IOException var11) {
            log.error("加载节点配置文件失败");
        } finally {
            try {
                if(in != null) {
                    in.close();
                }
            } catch (IOException var10) {
                log.error("关闭node.properties失败", var10);
            }

        }

        log.info("节点列表:" + this.nodeProperties);
    }

    public void notifyConfigInfoChange(String dataId, String group) {
        Enumeration enu = this.nodeProperties.propertyNames();

        while(enu.hasMoreElements()) {
            String address = (String)enu.nextElement();
            if(!address.contains(SystemConfig.LOCAL_IP)) {
                String urlString = this.generateNotifyConfigInfoPath(dataId, group, address);
                String result = this.invokeURL(urlString);
                log.info("通知节点" + address + "分组信息改变：" + result);
            }
        }

    }

    String generateNotifyConfigInfoPath(String dataId, String group, String address) {
        String specialUrl = this.nodeProperties.getProperty(address);
        String urlString = "http://" + address + "/diamond-server/notify.do";
        if(specialUrl != null && StringUtils.hasLength(specialUrl.trim())) {
            urlString = specialUrl;
        }

        urlString = urlString + "?method=notifyConfigInfo&dataId=" + dataId + "&group=" + group;
        return urlString;
    }

    private String invokeURL(String urlString) {
        HttpURLConnection conn = null;
        URL url = null;

        try {
            url = new URL(urlString);
            conn = (HttpURLConnection)url.openConnection();
            conn.setConnectTimeout(5000);
            conn.setReadTimeout(5000);
            conn.setRequestMethod("GET");
            conn.connect();
            InputStream urlStream = conn.getInputStream();
            StringBuilder sb = new StringBuilder();
            BufferedReader reader = null;

            String line;
            try {
                reader = new BufferedReader(new InputStreamReader(urlStream));
                line = null;

                while((line = reader.readLine()) != null) {
                    sb.append(line);
                }
            } finally {
                if(reader != null) {
                    reader.close();
                }

            }

            line = sb.toString();
            return line;
        } catch (Exception var17) {
            log.error("http调用失败,url=" + urlString, var17);
        } finally {
            if(conn != null) {
                conn.disconnect();
            }

        }

        return "error";
    }
}
