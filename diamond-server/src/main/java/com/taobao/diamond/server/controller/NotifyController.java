//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.controller;

import com.taobao.diamond.server.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping({"/notify.do"})
public class NotifyController {
    @Autowired
    private ConfigService configService;

    public NotifyController() {
    }

    public ConfigService getConfigService() {
        return this.configService;
    }

    public void setConfigService(ConfigService configService) {
        this.configService = configService;
    }

    @RequestMapping(
            method = {RequestMethod.GET},
            params = {"method=notifyConfigInfo"}
    )
    public String notifyConfigInfo(@RequestParam("dataId") String dataId, @RequestParam("group") String group) {
        dataId = dataId.trim();
        group = group.trim();
        this.configService.loadConfigInfoToDisk(dataId, group);
        return "200";
    }
}
