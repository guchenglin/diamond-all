

package com.taobao.diamond.server.controller;

import com.taobao.diamond.server.service.ConfigService;
import com.taobao.diamond.server.service.DiskService;
import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ConfigServlet extends HttpServlet {
    private static final long serialVersionUID = 4339468526746635388L;
    private ConfigController configController;
    private ConfigService configService;
    private DiskService diskService;

    public ConfigServlet() {
    }

    public void init() throws ServletException {
        super.init();
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(this.getServletContext());
        this.configService = (ConfigService)webApplicationContext.getBean("configService");
        this.diskService = (DiskService)webApplicationContext.getBean("diskService");
        this.configController = new ConfigController();
        this.configController.setConfigService(this.configService);
        this.configController.setDiskService(this.diskService);
    }

    public String getRemortIP(HttpServletRequest request) {
        return request.getHeader("x-forwarded-for") == null?request.getRemoteAddr():request.getHeader("x-forwarded-for");
    }

    public void forward(HttpServletRequest request, HttpServletResponse response, String page, String basePath, String postfix) throws IOException, ServletException {
        RequestDispatcher requestDispatcher = request.getRequestDispatcher(basePath + page + postfix);
        requestDispatcher.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String probeModify = request.getParameter("Probe-Modify-Request");
        if(!StringUtils.hasLength(probeModify)) {
            throw new IOException("无效的probeModify");
        } else {
            String page = this.configController.getProbeModifyResult(request, response, probeModify);
            this.forward(request, response, page, "/jsp/", ".jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String group = request.getParameter("group");
        String dataId = request.getParameter("dataId");
        if(!StringUtils.hasLength(dataId)) {
            throw new IOException("无效的dataId");
        } else {
            String page = this.configController.getConfig(request, response, dataId, group);
            if(page.startsWith("forward:")) {
                page = page.substring(8);
                this.forward(request, response, page, "", "");
            } else {
                this.forward(request, response, page, "/jsp/", ".jsp");
            }

        }
    }
}
