//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.utils;

import java.util.List;

public class LdapPersonInfo {
    private String uid;
    private String firstName;
    private String lastName;
    private List cn;
    private String telephone;
    private String fax;
    private String mail;

    public LdapPersonInfo() {
    }

    public LdapPersonInfo(String uid, String firstName, String lastName, List cn, String telephone, String fax, String mail) {
        this.uid = uid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.cn = cn;
        this.telephone = telephone;
        this.fax = fax;
        this.mail = mail;
    }

    public String getFax() {
        return this.fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMail() {
        return this.mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelephone() {
        return this.telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getUid() {
        return this.uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public List getCn() {
        return this.cn;
    }

    public void setCn(List cn) {
        this.cn = cn;
    }
}
