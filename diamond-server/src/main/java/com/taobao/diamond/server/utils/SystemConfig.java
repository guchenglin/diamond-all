//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.taobao.diamond.server.utils;

import com.taobao.diamond.utils.ResourceUtils;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Enumeration;
import java.util.Properties;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class SystemConfig {
    private static final Log log = LogFactory.getLog(SystemConfig.class);
    private static int dumpConfigInterval = 600;
    public static final String LOCAL_IP = getHostAddress();

    public SystemConfig() {
    }

    public static int getDumpConfigInterval() {
        return dumpConfigInterval;
    }

    private static String getHostAddress() {
        String address = "127.0.0.1";

        try {
            Enumeration en = NetworkInterface.getNetworkInterfaces();

            while(en.hasMoreElements()) {
                NetworkInterface ni = (NetworkInterface)en.nextElement();
                Enumeration ads = ni.getInetAddresses();

                while(ads.hasMoreElements()) {
                    InetAddress ip = (InetAddress)ads.nextElement();
                    if(!ip.isLoopbackAddress() && ip.isSiteLocalAddress()) {
                        return ip.getHostAddress();
                    }
                }
            }
        } catch (Exception var5) {
            ;
        }

        return address;
    }

    static {
        InputStream in = null;

        try {
            in = ResourceUtils.getResourceAsStream("system.properties");
            Properties props = new Properties();
            props.load(in);
            dumpConfigInterval = Integer.parseInt(props.getProperty("dump_config_interval", "600"));
        } catch (IOException var10) {
            log.error("加载system.properties出错", var10);
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException var9) {
                    log.error("关闭system.properties出错", var9);
                }
            }

        }

    }
}
