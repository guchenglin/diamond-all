package com.taobao.diamond.server.utils;

public class GlobalCounter
{
 private static GlobalCounter instance = new GlobalCounter();

 private long count = 0L;

 public static GlobalCounter getCounter()
 {
  return instance;
 }

 public synchronized long decrementAndGet()
 {
  if (this.count == -9223372036854775808L) {
   this.count = 0L;
  }
  else
   this.count -= 1L;
  return this.count;
 }

 public synchronized long get()
 {
  return this.count;
 }

 public synchronized void set(long value)
 {
  this.count = value;
 }
}