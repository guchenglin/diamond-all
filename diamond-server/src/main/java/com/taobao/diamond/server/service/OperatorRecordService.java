package com.taobao.diamond.server.service;

import com.taobao.diamond.utils.ResourceUtils;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Properties;
import javax.annotation.PostConstruct;
import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Service;

@Service
public class OperatorRecordService
{
    private static final String JDBC_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final int MAX_ROWS = 10000;
    private static final int QUERY_TIMEOUT = 2;
    private JdbcTemplate jt;

    private static String ensurePropValueNotNull(String srcValue)
    {
        if (srcValue == null) {
            throw new IllegalArgumentException("property is illegal:" + srcValue);
        }

        return srcValue;
    }

    public JdbcTemplate getJdbcTemplate()
    {
        return this.jt;
    }

    @PostConstruct
    public void initDataSource()
            throws Exception
    {
        Properties props = ResourceUtils.getResourceAsProperties("config.properties");
        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName("com.mysql.jdbc.Driver");
        ds.setUrl(ensurePropValueNotNull(props.getProperty("db.url")));
        ds.setUsername(ensurePropValueNotNull(props.getProperty("db.user")));
        ds.setPassword(ensurePropValueNotNull(props.getProperty("db.password")));
        ds.setInitialSize(Integer.parseInt(ensurePropValueNotNull(props.getProperty("db.initialSize"))));
        ds.setMaxActive(Integer.parseInt(ensurePropValueNotNull(props.getProperty("db.maxActive"))));
        ds.setMaxIdle(Integer.parseInt(ensurePropValueNotNull(props.getProperty("db.maxIdle"))));
        ds.setMaxWait(Long.parseLong(ensurePropValueNotNull(props.getProperty("db.maxWait"))));
        ds.setPoolPreparedStatements(Boolean.parseBoolean(ensurePropValueNotNull(props.getProperty("db.poolPreparedStatements"))));

        this.jt = new JdbcTemplate();
        this.jt.setDataSource(ds);

        this.jt.setMaxRows(10000);

        this.jt.setQueryTimeout(2);
    }
    public void addOperatorRecord(final String userName, final String operator, final String dataId, final String groupId, final String oldConfig, final String newConfig, final Timestamp now) {
        this.jt.update("insert into operator_snapshot (user_name,operator_type,data_id,group_id,old_config,new_config,gmt_create,gmt_modified) values(?,?,?,?,?,?,?,?)", new PreparedStatementSetter()
        {
            public void setValues(PreparedStatement ps) throws SQLException
            {
                int index = 1;
                ps.setString(index++, userName);
                ps.setString(index++, operator);
                ps.setString(index++, dataId);
                ps.setString(index++, groupId);
                ps.setString(index++, oldConfig);
                ps.setString(index++, newConfig);
                ps.setTimestamp(index++, now);
                ps.setTimestamp(index++, now);
            }
        });
    }
}